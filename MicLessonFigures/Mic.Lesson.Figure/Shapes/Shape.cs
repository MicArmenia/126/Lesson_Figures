﻿using System;

namespace Mic.Lesson.Figure.Shapes
{
    abstract class Shape : IDrawable
    {
        protected Shape()
        {
            width = 5;
            height = 5;
            Color = ConsoleColor.White;
        }

        public ConsoleColor Color { get; set; }

        protected int width;
        public virtual int Width
        {
            get => width;
            set
            {
                if (value < 0)
                    width = 5;
                else
                    width = value;
            }
        }

        protected int height;
        public virtual int Height
        {
            get => height;
            set
            {
                if (value < 0)
                    height = 5;
                else
                    height = value;
            }
        }

        //public virtual void Draw() { }
        public abstract void Draw();
    }
}