﻿using System;

namespace Mic.Lesson.Figure.Tables
{
    class Table : IDrawable
    {
        public Table()
        {
            Color = ConsoleColor.White;
        }

        public ConsoleColor Color { get; set; }

        private int columns;
        public int Columns
        {
            get { return columns; }
            set
            {
                if (value < 0)
                    columns = 5;
                else
                    columns = value;
            }
        }

        private int rows;
        public int Rows
        {
            get { return rows; }
            set
            {
                if (value < 0)
                    rows = 5;
                else
                    rows = value;
            }
        }

        public void Draw()
        {
            for (int i = 0; i <= rows; i++)
            {
                for (int j = 0; j <= columns; j++)
                {
                    if (j == columns - 1 && i == 1)
                        Console.Write("|__");
                    if (i == 0)
                    {
                        Console.WriteLine(new string('_', columns * 3 + 1));
                        i++;
                    }

                    else if (j < columns)
                    {

                        Console.Write("|__");
                    }
                    else
                    {
                        Console.Write('|');
                    }
                }

                Console.WriteLine();
            }
        }
    }
}

