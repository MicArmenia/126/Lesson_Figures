﻿using System;

namespace Mic.Lesson.Figure
{
    interface IDrawable
    {
        void Draw();
        ConsoleColor Color { get; set; }
    }
}