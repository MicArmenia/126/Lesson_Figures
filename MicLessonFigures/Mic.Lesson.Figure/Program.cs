﻿using Mic.Lesson.Figure.Shapes;
using Mic.Lesson.Figure.Shapes.Lines;
using Mic.Lesson.Figure.Shapes.Rectangles;
using Mic.Lesson.Figure.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figure
{
    class Program
    {
        static void Main(string[] args)
        {
            Shape shape = new Square
            {
                Height = 10,
                Width = 3,
                Color = ConsoleColor.Yellow
            };
            Print(shape);

            Table table = new Table()
            {
                Columns = 15,
                Rows = 7,
                Color = ConsoleColor.Red
            };
            Print(table);
            
            Console.ReadLine();
        }

        static void Print(IDrawable drawable)
        {
            string header = $"***** {drawable.GetType().Name} *****";
            Console.WriteLine(header);
            Console.WriteLine();

            ConsoleColor defaultColor = Console.ForegroundColor;
            Console.ForegroundColor = drawable.Color;

            drawable.Draw();

            Console.ForegroundColor = defaultColor;


            Console.WriteLine();
            Console.WriteLine(new string('*', header.Length));
        }

        //static void Print(Table table)
        //{
        //    string header = $"***** {table.GetType().Name} *****";
        //    Console.WriteLine(header);
        //    Console.WriteLine();

        //    table.Drow();

        //    Console.WriteLine();
        //    Console.WriteLine(new string('*', header.Length));
        //}

        //static void Print(Shape shape)
        //{
        //    string header = $"***** {shape.GetType().Name} *****";
        //    Console.WriteLine(header);
        //    Console.WriteLine();

        //    shape.Draw();

        //    Console.WriteLine();
        //    Console.WriteLine(new string('*', header.Length));
        //}
    }
}
